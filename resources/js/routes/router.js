import Vue from 'vue'
import VueRouter from 'vue-router';

Vue.use(VueRouter)

import Principal from '../components/App'
import Home from '../components/Home'
import LayoutD from '../components/layouts/layout'
import Dashboard from '../components/layouts/dashboard'

let isLogged = true;

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Principal',
      component: Principal,
      children: [
        {
          path: '/home',
          name: 'home',
          component: Home
        },
        {
          path: '/contacts',
          name: 'contacts',
          component: {template: '<div>Área de contactos</div>'}
        },
        {
          path: '/info',
          name: 'info',
          component: {template: '<div>Área de información</div>'}
        },
        { path: '/', redirect: '/home' }
      ]
    },

    {
      path: '/',
      name: 'layout',
      component: LayoutD,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: '/users',
          name: 'users',
          component: {template: '<div>Área de usuarios</div>'}
        },
        { path: '/', redirect: '/dashboard' }
      ]
    },
  ]
})

export default router
